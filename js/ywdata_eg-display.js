/**
 * Created by cenpas on 15/03/2018.
 */
$(document).ready(function() {

    $('#info02_btn').on('click', function (e) {
        $("#info02").toggle();
    });
    $('#id_admin').on('click', function (e) {
        $('#intro_pega_panel').toggle();

    });
    $('#reset_btn, #reset_btn2').on('click', function (e) {
        // reload the page - crude but effective for now
        window.location.reload();
    });
    $('#excel_file').change(function () {
        // 'excel_file' is the id of the input button

        // 'post_xlsx_file' is the id of the form
        $('#post_xlsx_file').submit();
    });
    $('#post_xlsx_file').on('submit', function(event){
        event.preventDefault();
        var val = '';
        var file_data = $('#excel_file').prop('files')[0];
        var form_data = new FormData();
        form_data.append('excel_file', file_data);
        $.ajax({
            type: 'post',
            url: 'fetch_xlsx_data.php',
            data: form_data, //new FormData(this),
            contentType:false,
            processData:false,
            dataType: "json",
            success:function(mydata){
                // reset all arrays
                plot_means = [];
                plot_maxs = [];
                plot_iqrs = [];
                plot_data_all = [];
                plot_data_index = [];
                plot_nums = [];
                // Now get the data
                plot_title = mydata.title;
                plot_subtitle = mydata.subtitle;
                plot_count = mydata.num_cols;
                plot_data_num = mydata.num_rows;
                plot_names = mydata.data_column_titles;
                plot_data_index = mydata.data_index;

                for(var i = 0 ; i < plot_count ; i++){
                    plot_means.push(parseFloat(mydata.means[i]));
                    plot_maxs.push(parseFloat(mydata.data_column_max[i]));
                    //iqrs = [];
                    //for(var j = 0 ; j < 5 ; j++){
                    //    iqrs.push(parseFloat(mydata.iqrs[i][j]));
                    //}
                    //plot_iqrs.push(iqrs);
                    plot_iqrs.push({
                        low: parseFloat(mydata.iqrs[i][0]),
                        q1: parseFloat(mydata.iqrs[i][1]),
                        median: parseFloat(mydata.iqrs[i][2]),
                        q3: parseFloat(mydata.iqrs[i][3]),
                        high: parseFloat(mydata.iqrs[i][4]),
                        mean: parseFloat(mydata.means[i]),
                        num: parseFloat(mydata.data_nums[i]),
                        code: plot_names[i],
                        mark: 50
                    });
                    plot_nums.push({
                       x: i,
                       y: 3,
                       num: parseFloat(mydata.data_nums[i])
                    });

                    data_set = [];
                    for(j = 0 ; j < plot_data_num ; j++){
                        data_set.push(parseFloat(mydata.data_all[i][j]));
                    }
                    plot_data_all.push(data_set);

                }

                update_hist_select_data(plot_count,1);

                // load the data into the plots
                draw_histogram_chart(true);
                draw_boxplot_chart(true);

                //$('#result').html(JSON.stringify(mydata));

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

                //$('#result').html('<p>status code: ' + jqXHR.status + '</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>' + jqXHR.responseText + '</div>');
                console.log('jqXHR:');
                console.log(jqXHR);
                console.log('textStatus:');
                console.log(textStatus);
                console.log('errorThrown:');
                console.log(errorThrown);
            }
        });
        //fetch_xlsx_data(val);
    });


function read_xlsx_data_json(file){
    //alert(file.name);
    var reader = new FileReader();

    reader.onload = function (event, file) {
        the_url = event.target.result;

        //of course using a template library like handlebars.js is a better solution than just inserting a string
        json_text = reader.result;

        //console.log(json_text);

        // a clear text json file would start with a '{'
        if(json_text.charAt(0) != '{') {
            json_text = base64_decode(json_text);
            //console.log(json_text);
        }

        try {
            work_obj = JSON.parse(json_text);
            //console.log(work_obj);
        } catch (err) {
            alert("An error occured parsing the file " + file.name + "\nWith Message: " + err.message);
        }

        pega_modules_obj = work_obj.pega_modules_obj;
        pega_staff_load_obj = work_obj.pega_staff_load_obj;

        personal_staff_index = -1;
        personal_staff_name = work_obj.name_obj;

        processLoadedJSON();

        // Hide the no-json notice
        $("div[name='no_json_info']").hide();

        // Put the name on the sheet
        $("#name_in_json_div").show();
        $("#name_in_json").html('Infomation displayed for: ' + personal_staff_name);

        if(personal_staff_index < 0){
            // Populate and Display the selection box
            $("#staff_name_select_div").show();
            var staff_select_options = "";
            staff_select_options = '<option value="-1" selected >-- Selects a staff name --</option>\n';

            for (var i = 0, len = staff_names.length; i < len; i++) {
                staff_select_options += '<option value="' + i + '" >' + staff_names[i] + '</option>\n';
            }
            $('#staff_name_select').html(staff_select_options);
        }else{
            $("#staff_name_select_div").hide();
        }

        if($('#all_module_lists').is(":visible") === false){toggle_all_module_lists();}
        if($('#all_staff_charts').is(":visible") === false){toggle_all_staff_charts();}

    };

    //when the file is read it triggers the onload event above.
    reader.readAsText(file);
}

});
