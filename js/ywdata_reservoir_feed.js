/**
 * Created by cenpas on 15/05/2018.
 */
var reservoir_feed;
var keys =[];
var mymap;
$(document).ready(function () {

    mymap = L.map('mapid').setView([53.7967969,-1.5328012], 10);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 20,
        id: 'mapbox.streets-satellite',
        accessToken: 'pk.eyJ1IjoiY2VucGFzIiwiYSI6ImNpcTQ1Y3ZtdjAwNnNoeG0yaW1nenF2bTgifQ.Vj6p6RhvLVgl6GXx8BWAmA'
        //mapbox.streets
        //mapbox.light
        //mapbox.dark
        //mapbox.satellite
        //mapbox.streets-satellite
        //mapbox.wheatpaste
        //mapbox.streets-basic
        //mapbox.comic
        //mapbox.outdoors
        //mapbox.run-bike-hike
        //mapbox.pencil
        //mapbox.pirates
        //mapbox.emerald
        //mapbox.high-contrast
    }).addTo(mymap);

    var popup = L.popup();

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(mymap);
    }

    mymap.on('click', onMapClick);

    $.ajax({
        type: "GET",
        url: "./data/ReservoirsJSON.json",
        dataType: "json",
        success: function (data) {
            processJSONData(data);
        }
    });
});

function processJSONData(allJSON) {
    reservoir_feed = allJSON;

    for (var x in reservoir_feed[0]) {
        keys.push(x);
    }
    console.log(reservoir_feed);
    console.log(keys);

    // Put this on the map
    for(var i = 0; i < reservoir_feed.length; i++){
        var lat = reservoir_feed[i]['Latitude'];
        var lon = reservoir_feed[i]['Longitude'];
        if((typeof lat != 'undefined') && (typeof lon != 'undefined')) {
            var marker = L.marker([lat, lon]).addTo(mymap);
            var name = reservoir_feed[i]['Name'];
            var desc = reservoir_feed[i]['Description'];
            var url = reservoir_feed[i]['URL'];
            marker.bindPopup("<b>" + name + "</b><br>" + desc + "<BR>" + url);
        }
    }
}

