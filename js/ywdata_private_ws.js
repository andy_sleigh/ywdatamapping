/**
 * Created by cenpas on 15/05/2018.
 */
var lines_obj;
var lines_arr;
var headers;
var mymap;
var postoutcode_lookup;
var postcode_boundaries;

$(document).ready(function () {
    mymap = L.map('mapid').setView([53.7967969,-1.5328012], 10);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 20,
        id: 'mapbox.streets-satellite',
        accessToken: 'pk.eyJ1IjoiY2VucGFzIiwiYSI6ImNpcTQ1Y3ZtdjAwNnNoeG0yaW1nenF2bTgifQ.Vj6p6RhvLVgl6GXx8BWAmA'
        //mapbox.streets
        //mapbox.light
        //mapbox.dark
        //mapbox.satellite
        //mapbox.streets-satellite
        //mapbox.wheatpaste
        //mapbox.streets-basic
        //mapbox.comic
        //mapbox.outdoors
        //mapbox.run-bike-hike
        //mapbox.pencil
        //mapbox.pirates
        //mapbox.emerald
        //mapbox.high-contrast
    }).addTo(mymap);


    var popup = L.popup();

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(mymap);
    }

    mymap.on('click', onMapClick);

    $.when(
        $.ajax({
            type: "GET",
            url: "./data/private_water_supplies.csv",
            dataType: "text",
            success: function (supply_data) {
                private_ws_raw_data = supply_data
            }
        }),
        // Read it in
        $.get('./data/postcode-outcodes.csv', function(pc_data) {
            var allCSV = pc_data;

            postoutcode_lookup = {};
            var allTextLines = allCSV.split(/\r\n|\n/);
            var pc_headers = allTextLines[0].split(',');
            for (var i = 1; i < allTextLines.length; i++) {
                var data = allTextLines[i].split(',');
                var row = {};
                row['lat'] = data[2];
                row['lon'] = data[3];
                postoutcode_lookup[data[1]] = row;
            }
        }, 'text'),
        // Read in geoJSON of poscode areas for yorkshire
        $.get('./data/YWDistricts.json', function(pc_boundary_data) {
            postcode_boundaries = pc_boundary_data;
            // Put this on the map
            pc_layer = L.geoJSON(postcode_boundaries, {onEachFeature: onEachFeatureClick}).addTo(mymap);
        }, 'json')
    ).then(function(){
        processCSVData(private_ws_raw_data);
    });

    $("#postcode_show").click(function(event) {
        event.preventDefault();
        if(mymap.hasLayer(pc_layer)) {
            $(this).removeClass('selected');
            mymap.removeLayer(pc_layer);
        } else {
            mymap.addLayer(pc_layer);
            $(this).addClass('selected');
        }
    });
});

function onEachFeatureClick(feature, layer) {
    // does this feature have a property named "name"?
    if (feature.properties && feature.properties.name) {
        // get the area of this postcode area
        var polygon = feature.geometry;
        var area = turf.area(polygon);
        area /= (1000*1000);
        var center = turf.center(polygon);
        var center_string = "<BR>Center: lat: " + center.geometry.coordinates[1].toFixed(2) + " lon: " + center.geometry.coordinates[0].toFixed(2);
        layer.bindPopup(feature.properties.name + "\n<BR>Area: " + area.toFixed(2) + " km<sup>2</sup>" + center_string);
    }
}

function processCSVData(allCSV) {
    var allTextLines = allCSV.split(/\r\n|\n/);
    headers = allTextLines[0].split(',');
    lines_obj = [];
    lines_arr = [];
    for (var i = 1; i < allTextLines.length; i++) {
        var data = allTextLines[i].split(',');
        if (data.length == headers.length) {
            var row = {};
            var row_arr = []
            for (var j = 0; j < headers.length; j++) {
                //row.push(headers[j] + ':' + data[j]);
                row_arr.push(data[j]);
                row[headers[j]] = data[j];
            }
            lines_obj.push(row);
            lines_arr.push(row_arr);
        }
    }
    console.log(headers);
    console.log(lines_obj);
    console.log(lines_arr);

    // Put this on the map
    for(i = 0; i < lines_obj.length; i++){
        var lat = lines_obj[i]['Longitude'];// Wrong way round in the data file?
        var lon = lines_obj[i]['Latitude'];
        if((typeof lat != 'undefined') && (typeof lon != 'undefined')) {
            var marker = L.marker([parseFloat(lat), parseFloat(lon)]).addTo(mymap);
            var name = lines_obj[i]['Name of Supply'];
            var postcode = lines_obj[i]['Postcode'];
            var osgridref = lines_obj[i]['OS Grid Ref'];
            var source = lines_obj[i]['Source'];
            var use = lines_obj[i]['Type of Water Use'];

            // Example of overting grid ref to lat lon
            var gridref = OsGridRef.parse(osgridref);
            var pWgs84 = OsGridRef.osGridToLatLon(gridref);

            // Example of converting lat lon to OS Grid ref
            var p = new  LatLon(lat, lon);
            gridref = OsGridRef.latLonToOsGrid(p); //

            p = new LatLon(pWgs84.lat, pWgs84.lon);
            var gridref2 = OsGridRef.latLonToOsGrid(p); //

            var text = "<b>" + name + "</b><br>";
            text += "Lat,Lon: " + lat + ", " + lon + "<BR>";
            text += "Source: " + source + "<BR>";
            text += "Use: " + use + "<BR>";
            text += "Postcode: " + postcode + "<BR>";
            text += "OS Grid ref: " + osgridref + "<BR>";
            text += "Lat,Lon from OS Grid ref: " + pWgs84.lat.toFixed(4) + ", " + pWgs84.lon.toFixed(4) + "<BR>";
            text += "OS Grid ref from Lat,Lon (re converted): " + gridref2.toString() + "<BR>";
            text += "OS Grid ref from Lat,Lon (from data) " + gridref.toString() + "<BR>";

            var post_outcode = postcode.substr(0,postcode.indexOf(' '));
            var pcLatLon = postoutcode_lookup[post_outcode];
            text += "Lat,Lon from Post outcode " + pcLatLon.lat + ", " + pcLatLon.lon;
            marker.bindPopup(text);
        }
    }

}


function postoutcodeGetLatLon(post_outcode){
    var LatLon = {};
    LatLon = postoutcode_lookup[post_outcode];
    return LatLon;
}

